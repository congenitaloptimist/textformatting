﻿using System;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.TextFormatting;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextFormatting
{
    static public class GlyphRunHelper
    {
        static public double GetAdvanceWidth(this GlyphRun run)
        {
            double advance = 0;
            if (run.AdvanceWidths != null)
            {
                foreach (double glyphAdvance in run.AdvanceWidths)
                    advance += glyphAdvance;
            }
            return advance;
        }

        static public double GetAscent(this GlyphRun run)
        {
            // for sideways text, origin is in the middle of the character cell
            if (run.IsSideways)
                return run.FontRenderingEmSize * run.GlyphTypeface.Height / 2.0;

            return run.FontRenderingEmSize * run.GlyphTypeface.Baseline;
        }

        static public double GetHeight(this GlyphRun run)
        {
            return run.FontRenderingEmSize * run.GlyphTypeface.Height;
        }

        static public bool IsLeftToRight(this GlyphRun run)
        {
            return (run.BidiLevel & 1) == 0;
        }

        static public Rect GetBoundsOnBaseline(this GlyphRun run)
        {
            if (IsLeftToRight(run))
                return new Rect(0, -run.GetAscent(), run.GetAdvanceWidth(), run.GetHeight());

            double advanceWidth = run.GetAdvanceWidth();
            return new Rect(-advanceWidth, -run.GetAscent(), advanceWidth, run.GetHeight());
        }

        static public Rect GetBounds(this GlyphRun run)
        {
            if (run.IsLeftToRight())
                return new Rect(0, -run.GetAscent(), run.GetAdvanceWidth(), run.GetHeight());

            double advanceWidth = run.GetAdvanceWidth();
            return new Rect(-advanceWidth, -run.GetAscent(), advanceWidth, run.GetHeight());
        }

        static public String GetRunText(this GlyphRun run)
        {
            StringBuilder sb = new StringBuilder();
            foreach (char c in run.Characters)
            {
                sb.Append(c);
            }
            return sb.ToString();
        }
    }
}
