﻿using System;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.TextFormatting;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextFormatting
{
    static public class IndexedGlyphRunHelper
    {
        static public double GetAdvanceWidth(this IndexedGlyphRun run)
        {
            return run.GlyphRun.GetAdvanceWidth(); ;
        }

        static public double GetAscent(this IndexedGlyphRun run)
        {
            return run.GlyphRun.GetAscent();
        }

        static public double GetHeight(this IndexedGlyphRun run)
        {
            return run.GlyphRun.GetHeight();
        }

        static public bool IsLeftToRight(this IndexedGlyphRun run)
        {
            return run.GlyphRun.IsLeftToRight();
        }

        static public Rect GetBoundsOnBaseline(this IndexedGlyphRun run)
        {
            return run.GlyphRun.GetBoundsOnBaseline();
        }

        static public Rect GetBounds(this IndexedGlyphRun run)
        {
            return run.GlyphRun.GetBounds();
        }

        static public String GetRunText(this IndexedGlyphRun run)
        {
            return run.GlyphRun.GetRunText();
        }


    }
}
