﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.TextFormatting;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextFormatting
{
    static public class TextLineHelper
    {
        static public int GetVisualLength(this TextLine line)
        {
            //int length = 0;
            //IEnumerable<IndexedGlyphRun> runs = line.GetIndexedGlyphRuns();
            //foreach (IndexedGlyphRun run in runs)
            //    length += run.TextSourceLength;
            //return length;

            // NewlineLength is > 0 when we have line break or paragraph end
            return line.Length - line.NewlineLength;
        }

        static public void SpanTest(this TextLine line)
        {
            IList<System.Windows.Media.TextFormatting.TextSpan<System.Windows.Media.TextFormatting.TextRun>> spans = line.GetTextRunSpans();
            foreach (System.Windows.Media.TextFormatting.TextSpan<System.Windows.Media.TextFormatting.TextRun> span in spans)
            {
                System.Windows.Media.TextFormatting.TextRun trun = span.Value;
            }
        }

        static public CharacterHit GetFirstCharacterHit(this TextLine line)
        {
            // FIXME: use this instead
            // return new CharacterHit(line.GetLineStartIndex(), 0);
            return line.GetCharacterHitFromDistance(0.0);
        }

        static public CharacterHit GetLastCharacterHit(this TextLine line)
        {
            // NOTE: line.Length may contain paragraph/line break, how is that accounted here?
            CharacterHit ch = line.GetFirstCharacterHit( );
            do
            {
                CharacterHit nh = line.GetNextCaretCharacterHit(ch);
                if (nh == ch)
                    break;
                ch = nh;
            }
            while (true);

            return ch;
        }

        static public int GetLineStartIndex(this TextLine line)
        {
            IEnumerable<IndexedGlyphRun> runs = line.GetIndexedGlyphRuns();
            if(0 == runs.Count())
            {
                return line.GetFirstCharacterHit().FirstCharacterIndex;
            }

            IndexedGlyphRun run = runs.First();
            // FIXME: what if there are no runs? Use end of last line + 1 ?
            if(null != run)
                return run.TextSourceCharacterIndex;
            return 0;
        }

        static public int GetLineEndIndex(this TextLine line)
        {
            //IEnumerable<IndexedGlyphRun> runs = line.GetIndexedGlyphRuns();
            //IndexedGlyphRun run = runs.Last();
            //if(null != run)
            //return run.TextSourceCharacterIndex + run.TextSourceLength;

            return line.GetLineStartIndex() + line.GetVisualLength();
        }

        static public String GetLineText(this TextLine line)
        {
            StringBuilder sb = new StringBuilder();

            IEnumerable<IndexedGlyphRun> runs = line.GetIndexedGlyphRuns();
            foreach (IndexedGlyphRun run in runs)
            {
                foreach (char c in run.GlyphRun.Characters)
                {
                    sb.Append(c);
                }
            }
            return sb.ToString();
        }


        static public Rect GetGlypRunRect(this TextLine line, IndexedGlyphRun run)
        {
            IList<TextBounds> rbounds = line.GetTextBounds(run.TextSourceCharacterIndex, run.TextSourceLength);
            if(rbounds.Count > 0)
            {
                if (rbounds.Count > 1)
                {
                    // We did not expect that
                    Console.WriteLine("Multiple bounds were calculated for run '{0}'", run.GetRunText());
                }
                return rbounds[0].Rectangle;
            }

            Console.WriteLine("No bounds were calculated for run '{0}'", run.GetRunText());
            return Rect.Empty;
        }

        /*
        static public bool GetGlyphLeft(this TextLine line, int index, out double x)
        {
            x = 0;

            IEnumerable<IndexedGlyphRun> runs = line.GetIndexedGlyphRuns();
            foreach (IndexedGlyphRun run in runs)
            {
                int last = (run.TextSourceCharacterIndex + run.TextSourceLength);
                if (run.TextSourceCharacterIndex <= index && index < last)
                {
                    // The character is in this run 
                    Rect rect = line.GetGlypRunRect(run);
                    x = rect.Left;
                    for( int i = 0; i < run.TextSourceLength; ++i )
                    {
                        if ((run.TextSourceCharacterIndex + i) == index)
                            break;

                        x += run.GlyphRun.AdvanceWidths[i];
                    }
                    return true;
                }
            }
            return false;
        }

        static public bool GetGlyphLeftRight(this TextLine line, int index, out double l, out double r)
        {
            l = 0;
            r = 0;

            IEnumerable<IndexedGlyphRun> runs = line.GetIndexedGlyphRuns();
            foreach (IndexedGlyphRun run in runs)
            {
                int last = (run.TextSourceCharacterIndex + run.TextSourceLength);
                if (run.TextSourceCharacterIndex <= index && index < last)
                {
                    // The character is in this run 
                    Rect rect = line.GetGlypRunRect(run);
                    l = rect.Left;
                    for (int i = 0; i < run.TextSourceLength; ++i)
                    {
                        if ((run.TextSourceCharacterIndex + i) == index)
                        {
                            // FIXME: Right-to-left direction
                            r = l + run.GlyphRun.AdvanceWidths[i];
                            break;
                        }
                        l += run.GlyphRun.AdvanceWidths[i];
                    }
                    return true;
                }
            }
            return false;
        }
        */
    }
}
