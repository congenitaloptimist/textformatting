﻿using System;
using System.Collections.Generic;
using System.Windows.Media.TextFormatting;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextFormatting
{
    static public class CharacterHitHelper
    {
        public static bool IsValid( this CharacterHit hit )
        {
            return hit.FirstCharacterIndex >= 0;
        }

        public static int ToIndex(this CharacterHit hit)
        {
            return hit.FirstCharacterIndex + hit.TrailingLength;
        }
    }
}
