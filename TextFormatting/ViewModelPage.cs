﻿using System;
using System.Windows.Input;
using System.ComponentModel;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextFormatting
{
    public class ViewModelPage : ViewModel
    {
        public ViewModelPage()
        {
            ViewModelObjectText text = new ViewModelObjectText() { X = 26, Y = 44, W = 200, H = 160 };
            text.TextFlow.Add(new ViewModelTextParagraph());
            text.TextFlow.Add(new ViewModelTextRun( "Here is "));
            text.TextFlow.Add(new ViewModelTextRun("some") { SuperScript = true });
            text.TextFlow.Add(new ViewModelTextRun(" text in the paragraph"));
            text.TextFlow.Add(new ViewModelTextLineBreak());
            text.TextFlow.Add(new ViewModelTextRun("and now we insert a "));
            text.TextFlow.Add(new ViewModelTextRun("line break.") { Bold = true, FontSize = 14, FontFamily = "Times New Roman" });
            text.TextFlow.Add(new ViewModelTextParagraph());
            text.TextFlow.Add(new ViewModelTextRun("Now, can we "));
            text.TextFlow.Add(new ViewModelTextRun("format") { Bold = true, LetterSpacing = 4 });
            text.TextFlow.Add(new ViewModelTextRun(" this text and maintain "));
            text.TextFlow.Add(new ViewModelTextRun("MVVM?") { Bold = true, Italic = true });
            text.TextFlow.Add(new ViewModelTextParagraph());
            text.TextFlow.Add(new ViewModelTextRun("Find the content hardcoded in class "));
            text.TextFlow.Add(new ViewModelTextRun("ViewModelPage") { FontFamily = "Courier New" });
            text.TextFlow.Add(new ViewModelTextRun(". We use System.Windows.Media.TextFormatting to process and render text."));

            text.MakeIndex();
            Objects.Add(text);

            Objects.Add(new ViewModelObjectRect() { X = 123, Y = 244, W = 100, H = 60 });
            Objects.Add(new ViewModelObjectEllipse() { X = 30, Y = 344, W = 60, H = 60 });
            Objects.Add(new ViewModelObjectRect() { X = 26, Y = 455, W = 100, H = 60 });
        }

        public ObservableCollection<ViewModelObject> Objects
        {
            get { return m_objects; }
        }

        public double W
        {
            get { return m_W; }
            set { if (m_W != value) { m_W = value; OnPropertyChanged(); } }
        }

        public double H
        {
            get { return m_H; }
            set { if (m_H != value) { m_H = value; OnPropertyChanged(); } }
        }

        #region Command_Flip
        DelegateCommand m_Flip;
        public ICommand Flip
        {
            get
            {
                if (m_Flip == null)
                    m_Flip = new DelegateCommand(CommandFlipExecuted, CommandFlipCanExecute);
                return m_Flip;
            }
        }

        private void CommandFlipExecuted(object parameter)
        {
            double h = W;
            W = H;
            H = h;
        }

        private bool CommandFlipCanExecute(object parameter)
        {
            return true;
        }
        #endregion

        double m_W;
        double m_H;

        ObservableCollection<ViewModelObject> m_objects = new ObservableCollection<ViewModelObject>();
    }
}
