﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Media;
using System.Windows.Media.TextFormatting;
using System.Drawing;

namespace TextFormatting
{
    /// <summary>
    /// Implementation of System.Windows.Media.TextFormatting.TextRunProperties
    /// </summary>
    public class TextRunPropertiesAdapter : TextRunProperties
    {
        public TextRunPropertiesAdapter(ViewModelText model)
        {
            m_model = model;
        }

        public override Typeface Typeface
        {
            get 
            {
                if (m_typeface == null)
                {
                    FontFamily family = new FontFamily(m_model.State.FontFamily);

                    m_typeface = new Typeface(family, (m_model.State.Italic) ? FontStyles.Italic : FontStyles.Normal, (m_model.State.Bold) ? FontWeights.Bold: FontWeights.Regular, FontStretches.Normal);
                }
                return m_typeface; 
            }
        }

        public override double FontRenderingEmSize
        {
            get
            {
                if(m_model.State.SuperScript)
                    return m_model.State.FontSize * 0.6;
                return m_model.State.FontSize;
            }
        }

        public override double FontHintingEmSize
        {
            get
            {
                if (m_model.State.SuperScript)
                    return m_model.State.FontSize * 0.6;
                return m_model.State.FontSize;
            }
        }

        public override TextDecorationCollection TextDecorations
        {
            get
            {
                return m_decorations;
            }
        }

        public override System.Windows.Media.Brush ForegroundBrush
        {
            get 
            {
                if (m_foreground == null)
                    m_foreground = Brushes.Black;
                return m_foreground; 
            }
        }

        public override System.Windows.Media.Brush BackgroundBrush
        {
            get 
            {
                if (m_background == null)
                    m_background = Brushes.Transparent;
                return m_background; 
            }
        }

        public override BaselineAlignment BaselineAlignment
        {
            get
            {
                return BaselineAlignment.Baseline;
            }
        }

        public override CultureInfo CultureInfo
        {
            get
            {
                return CultureInfo.CurrentUICulture;
            }
        }

        public override TextRunTypographyProperties TypographyProperties
        {
            get
            {
                // currently unused
                return m_typography;
            }
        }

        public override TextEffectCollection TextEffects
        {
            // allows animation and transformation
            get
            {
                //if(null == m_effects)
                //{
                //    m_effects = new TextEffectCollection();
                //    m_effects.Add(new TextEffect(new TranslateTransform(0, 7), System.Windows.Media.Brushes.Red, null, 5, 2)); // Geometry.Empty
                //    m_effects.Add(new TextEffect(new ScaleTransform(1.5, 1.0), System.Windows.Media.Brushes.RoyalBlue, null, 7, 2));
                //}
                return m_effects;
            }
        }

        public override NumberSubstitution NumberSubstitution
        {
            get { return null; }
        }

        private ViewModelText m_model;
        private Typeface m_typeface;
        private System.Windows.Media.Brush m_foreground;
        private System.Windows.Media.Brush m_background;
        private TextRunTypographyProperties m_typography = null;
        private TextDecorationCollection m_decorations = null;
        private TextEffectCollection m_effects = null;
    }
}
