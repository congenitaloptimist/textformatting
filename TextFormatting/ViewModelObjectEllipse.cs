﻿using System;
using System.Windows.Media;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextFormatting
{
    public class ViewModelObjectEllipse : ViewModelObject
    {
        public ViewModelObjectEllipse( )
            : base( )
        {
            Color = Color.FromArgb(255, (byte)rnd.Next(256), (byte)rnd.Next(256), (byte)rnd.Next(256));
            Stroke = Colors.DarkBlue;
        }
    }
}
