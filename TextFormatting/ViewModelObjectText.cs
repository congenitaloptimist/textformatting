﻿using System;
using System.Windows.Media;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextFormatting
{
    public class ViewModelObjectText : ViewModelObject
    {
        public ViewModelObjectText( )
            : base( )
        {
            Color = Colors.WhiteSmoke;
            TextColor = Colors.Black;
        }

        public Color TextColor
        {
            get { return m_textcolor; }
            set { if (m_textcolor != value) { m_textcolor = value; TextBrush = new SolidColorBrush(value); OnPropertyChanged(); } }
        }

        public Brush TextBrush
        {
            get { return m_textbrush; }
            set { if (m_textbrush != value) { m_textbrush = value; OnPropertyChanged(); } }
        }

        public ObservableCollection<ViewModelText> TextFlow
        {
            get { return m_text; }
        }

        public int Count
        {
            get
            {
                int count = 0;
                foreach (ViewModelText text in TextFlow)
                {
                    if (text is ViewModelTextParagraph && text == TextFlow[0])
                        continue;

                    count += text.Length;
                }
                return count;
            }
        }

        public ViewModelTextParagraph GetParagraphAt(int index)
        {
            ViewModelTextParagraph paragraph = null;

            int i = 0;

            foreach (ViewModelText text in TextFlow)
            {
                if (text is ViewModelTextParagraph)
                    paragraph = (text as ViewModelTextParagraph);

                if (i >= index)
                    return paragraph;

                i += text.Length;
            }

            return paragraph;
        }

        public ViewModelText AtIndex(int index)
        {
            int i = 0;

            foreach (ViewModelText text in TextFlow)
            {
                if (text is ViewModelTextParagraph && text == TextFlow[0])
                    continue;

                if (i <= index && index < (i + text.Length))
                    return text;

                i += text.Length;
            }
            return null;
        }

        public bool MakeIndex( )
        {
            ViewModelTextParagraph paragraph = null;

            int index = 0;

            foreach(ViewModelText text in TextFlow)
            {
                if (text is ViewModelTextParagraph)
                {
                    // current paragraph to state
                    paragraph = (text as ViewModelTextParagraph);
                }

                text.Index = index;
                text.State.Paragraph = paragraph;

                if (text is ViewModelTextParagraph && text == TextFlow[0])
                    continue;

                index += text.Length;

                if (text is ViewModelTextRun)
                {
                    // style to state
                    text.State.Bold = (text as ViewModelTextRun).Bold;
                    text.State.Italic = (text as ViewModelTextRun).Italic;
                    text.State.FontFamily = (text as ViewModelTextRun).FontFamily;
                    text.State.FontSize = (text as ViewModelTextRun).FontSize;
                    text.State.LetterSpacing = (text as ViewModelTextRun).LetterSpacing;
                    text.State.SuperScript = (text as ViewModelTextRun).SuperScript;
                }
            }

            return true;
        }

        Color m_textcolor;
        Brush m_textbrush;

        ObservableCollection<ViewModelText> m_text = new ObservableCollection<ViewModelText>();
    }
}
