﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.TextFormatting;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TextFormatting
{
    /// <summary>
    /// Interaction logic for TextControl.xaml
    /// </summary>
    public partial class TextControl : FrameworkElement
    {
        public TextControl()
        {
            InitializeComponent();

            this.ClipToBounds = false;
        }

        public static readonly DependencyProperty TextModelProperty =
            DependencyProperty.Register("TextModel", typeof(ViewModelObjectText), typeof(TextControl),
            new FrameworkPropertyMetadata(null, ModelChanged));

        public ViewModelObjectText TextModel
        {
            get { return (ViewModelObjectText)GetValue(TextModelProperty); }
            set { SetValue(TextModelProperty, value); }
        }

        TextLayout m_layout;
        TextLayout Layout
        {
            get { return m_layout; }
            set { m_layout = value; }
        }

        static void ModelChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            TextControl target = d as TextControl;
            if (target == null)
                return;

            target.UpdateTextState();
        }

        void UpdateTextState( )
        {
            TextLayout layout = new TextLayout();
            layout.Run(TextModel);
            Layout = layout;
        }

        protected override void OnRender(DrawingContext context)
        {
            base.OnRender(context);

            Rect area = new Rect(0, 0, this.ActualWidth, this.ActualHeight);
            
            context.DrawRectangle(Brushes.WhiteSmoke, null, area);

            if (null == Layout)
                return;

            // for all lines
            foreach (TextLine line in Layout.TextLines)
            {
                double y = Layout.GetLinePosition(line);

                // vertical layout
                context.PushTransform(new TranslateTransform(0, y));

                // current line
                if (line.Extent != 0)
                {
                    // for all runs in line
                    IEnumerable<IndexedGlyphRun> runs = line.GetIndexedGlyphRuns();
                    foreach (IndexedGlyphRun run in runs)
                    {
                        // current run
                        Rect r = line.GetGlypRunRect(run);
                        if (!r.IsEmpty)
                        {
                            System.Windows.Media.Brush brush = Brushes.Black;
                            double dy = 0;

                            ViewModelText text = TextModel.AtIndex(run.TextSourceCharacterIndex);
                            if (null != text)
                            {
                                TextState state = text.State;
                                if (state.SuperScript)
                                    dy = - state.FontSize * 0.6;
                                /*
                                if (null != state && null != state.TextBrush)
                                    brush = MediaHelper.MakeBrush(state.TextBrush);
                                else
                                    brush = MediaHelper.MakeBrush(element.TextFlow.TextBrush);

                                if (null == m_background && null != state && null != state.FillBrush)
                                {
                                    System.Windows.Media.Brush backg = MediaHelper.MakeBrush(state.FillBrush);
                                    context.DrawRectangle(backg, null, r);
                                }
                                */
                            }

                            // render a single run
                            context.PushTransform(new TranslateTransform(r.Left, r.Top + line.Baseline + dy));
                            context.DrawGlyphRun(brush, run.GlyphRun);

                            //DrawDecorations(context, r, state, run);

                            context.Pop();
                        }
                    }

                    // to draw a whole line
                    // context.PushOpacity(0.2);
                    // line.Draw(context, new System.Windows.Point(0, 0), InvertAxes.None);
                    // context.Pop();
                }

                // vertical layout
                context.Pop();
            }
        }

        /*
        void DrawDecorations(DrawingContext context, Rect r, TextState state, IndexedGlyphRun run)
        {
            if (state.Font.HasStyle(FontStyle.Strikeout))
            {
                double dy = 0.2;
                double dw = 0.05;

                GlyphTypeface typeface = run.GlyphRun.GlyphTypeface;
                if (typeface != null)
                {
                    dy = typeface.StrikethroughPosition;
                    dw = typeface.StrikethroughThickness;
                }

                System.Windows.Point p1 = new System.Windows.Point(0, -dy * state.Font.Size.Value);
                System.Windows.Point p2 = new System.Windows.Point(r.Width, p1.Y);
                System.Windows.Media.Pen pen = MediaHelper.MakePen(state.TextBrush, dw * state.Font.Size.Value);
                context.DrawLine(pen, p1, p2);
            }

            if (state.Font.HasStyle(FontStyle.Underlined))
            {
                double dy = -0.1;
                double dw = 0.05;

                GlyphTypeface typeface = run.GlyphRun.GlyphTypeface;
                if (typeface != null)
                {
                    dy = typeface.UnderlinePosition;
                    dw = typeface.UnderlineThickness;
                }

                System.Windows.Point p1 = new System.Windows.Point(0, -dy * state.Font.Size.Value);
                System.Windows.Point p2 = new System.Windows.Point(r.Width, p1.Y);
                System.Windows.Media.Pen pen = MediaHelper.MakePen(state.TextBrush, dw * state.Font.Size.Value);
                context.DrawLine(pen, p1, p2);
            }
        }

        double GetEffectiveBaselineShift(Rect r, TextState state, IndexedGlyphRun run)
        {
            double y_shift = 0;
            if (state.Font.HasStyle(FontStyle.Superscript))
            {
                y_shift = -state.Font.Size.Value * 0.4;
            }
            else if (state.Font.HasStyle(FontStyle.Subscript))
            {
                y_shift = state.Font.Size.Value * 0.2;
            }
            return y_shift + state.Shift.Value;
        }
        */
    }
}
