﻿using System;
using System.Windows;
using System.Windows.Input;
using System.Windows.Controls;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextFormatting
{
    class ViewWorkArea : ScrollViewer
    {
        System.Windows.Point m_start;
        System.Windows.Point m_end;
        System.Windows.Point m_last;
        SelectionAdorner m_selection;

        protected override void OnMouseLeftButtonDown(MouseButtonEventArgs e)
        {
            if (IsMouseCaptured)
            {
                ReleaseMouseCapture();
                return;
            }

            m_start = e.GetPosition(this);
            m_end = e.GetPosition(this);
            m_last = e.GetPosition(this);

            Mouse.Capture(this);
        }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            if (!IsMouseCaptured)
                return;

            m_end = e.GetPosition(this);

            if (m_selection == null)
                m_selection = new SelectionAdorner(this, m_start);

            m_selection.End = m_end;
        }

        protected override void OnMouseLeftButtonUp(MouseButtonEventArgs e)
        {
            if (m_selection != null)
            {
                System.Windows.Rect r = new Rect(m_start, m_end);
                if (r.Width > 0 && r.Height > 0)
                    ZoomToRectangle(r);

                m_selection.Finish();
                m_selection = null;
            }
            ReleaseMouseCapture();
        }

        protected override void OnLostMouseCapture(MouseEventArgs e)
        {

        }

        void ZoomToRectangle(Rect r)
        {
            ViewDocumentPages content = FrameworkHelpers.FindVisualChild<ViewDocumentPages>(this);
            if (null == content)
                return;

            content.ZoomToRectangle(this, r);
        }
    }
}
