﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.TextFormatting;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextFormatting
{
    /**
     * Turns text model into formatted lines of text
     * 
     * Use: first Run( ) and then use TextLines
     */
    public class TextLayout
    {
        void Clear( )
        {
            m_lines.Clear();
            m_data.Clear();
        }

        public bool Run(ViewModelObjectText container)
        {
            Clear();

            if (null == container)
                return false;

            if(!Build(container, container.W))
                return false;
            
            return true;
        }

        public ObservableCollection<TextLine> TextLines
        {
            get { return m_lines; }
        }

        public double CharCount
        {
            get { return m_CharCount; }
        }

        public TextLine FirstLine( )
        {
            if(TextLines.Count == 0)
                return null;

            return TextLines.First();
        }

        public TextLine LastLine()
        {
            if (TextLines.Count == 0)
                return null;

            return TextLines.Last();
        }

        public TextLine PreviousLine(TextLine that)
        {
            TextLine prev = null;
            foreach (TextLine line in TextLines)
            {
                if (object.ReferenceEquals(line, that))
                    return prev;

                prev = line;
            }
            return null;
        }

        public TextLine NextLine(TextLine that)
        {
            bool found = false;
            foreach (TextLine line in TextLines)
            {
                if (found)
                    return line;
                if (object.ReferenceEquals(line, that))
                    found = true;
            }
            return null;
        }

        public double GetLinePosition(TextLine line)
        {
            return m_data[line].yposition;
        }

        public double GetLineBaselinePosition(TextLine line)
        {
            return m_data[line].yposition + line.Baseline;
        }

        public TextLine GetLineAtHeight(double y)
        {
            double yoffset = 0;
            foreach (TextLine line in TextLines)
            {
                yoffset = GetLinePosition(line);

                if (y <= (yoffset + line.Height))
                    return line;
            }
            return null;
        }

        bool Build(ViewModelObjectText flow, double width)
        {
            if (width < 0)
                return false;

            TextSourceAdapter source = new TextSourceAdapter(flow);

            TextLineBreak lastLineBreak = null;
            TextFormatter formatter = TextFormatter.Create(TextFormattingMode.Ideal);

            bool firstline = true;

            TextLine prev = null;
            ViewModelTextParagraph paragprev = null;
            ViewModelTextParagraph paragraph = flow.GetParagraphAt(0);

            int offset = 0;
            int total = flow.Count;

            double y = 0; // bottom of (previous) line, not including any extras

            while (offset < total)
            {
                ViewModelText element = flow.AtIndex(offset);
                if (null == element)
                {
                    throw new ApplicationException("Text element not found");
                }

                if (!Object.ReferenceEquals(paragraph, element.State.Paragraph))
                {
                    firstline = true;
                    paragprev = paragraph;
                    paragraph = element.State.Paragraph;
                    prev = null; // forget previous line on the paragraph change
                }

                // actual available width - area minus paragraph left and right indents
                double available = width;

                TextLine line = formatter.FormatLine(source, offset, available, new TextParagraphPropertiesAdapter(paragraph, new TextRunPropertiesAdapter(element), firstline), lastLineBreak); // m_cache
                m_lines.Add(line);
                lastLineBreak = line.GetTextLineBreak();

                // position for the new line

                double ytop = y;
                double ybase = ytop + line.Baseline;
                double ybot = ytop + line.Height;

                if (firstline)
                {
                    double extra = 0;

                    // if (null != paragprev)
                    //    extra += paragprev.SpaceAfter;

                    //extra += paragraph.SpaceBefore;

                    ytop += extra;
                    ybase += extra;
                    ybot += extra;
                }

                m_data[line] = new TextLineData()
                {
                    index = offset,
                    yposition = ytop
                };

                // increment
                y = ybot;
                prev = line;
                firstline = false;
                offset += line.Length;
            }

            m_CharCount = offset;
            return true;
        }

        struct TextLineData
        {
            public int index;
            public double yposition;
        }

        // TextRunCache m_cache = new TextRunCache();
        int m_CharCount = 0;

        ObservableCollection<TextLine> m_lines = new ObservableCollection<TextLine>();
        Dictionary<TextLine, TextLineData> m_data = new Dictionary<TextLine, TextLineData>();
    }
}
