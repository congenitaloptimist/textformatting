﻿using System;
using System.Windows.Input;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextFormatting
{
    public class ViewModelDocument : ViewModel
    {
        public ViewModelDocument( )
        {
            // populate for test
            Pages.Add(new ViewModelPage() { W = 595, H = 842 });
        }

        public ViewModelPage Page
        {
            get { return m_page; }
            set { if(m_page != value) m_page = value; }
        }

        public ObservableCollection<ViewModelPage> Pages
        {
            get { return m_pages; }
        }

        ViewModelPage m_page;
        ObservableCollection<ViewModelPage> m_pages = new ObservableCollection<ViewModelPage>( );
    }
}
