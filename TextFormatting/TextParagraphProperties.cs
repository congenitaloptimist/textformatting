﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Media;
using System.Windows.Media.TextFormatting;
using System.Drawing;

namespace TextFormatting
{
    /**
     * Implementation of TextParagraphProperties for TextSource
     */
    public class TextParagraphPropertiesAdapter : TextParagraphProperties
    {
        ViewModelTextParagraph m_paragraph;
        TextRunPropertiesAdapter m_runproperties;
        bool m_firstline;

        public TextParagraphPropertiesAdapter(ViewModelTextParagraph paragraph, TextRunPropertiesAdapter runproperties, bool firstline)
        {
            m_paragraph = paragraph;
            m_runproperties = runproperties;
            m_firstline = firstline;
        }

        public override bool AlwaysCollapsible
        {
            get { return base.AlwaysCollapsible; }
        }

        public override double DefaultIncrementalTab
        {
            get { return 40; }
        }

        public override TextRunProperties DefaultTextRunProperties
        {
            get { return m_runproperties; }
        }

        public override bool FirstLineInParagraph
        {
            get { return m_firstline; }
        }

        public override FlowDirection FlowDirection
        {
            get { return FlowDirection.LeftToRight; }
        }

        public override double Indent
        {
            get { return 0; }
        }

        public override double LineHeight
        {
            // return zero instead of m_paragraph.LineSpacing (we handle vertical layout separately)
            get { return 0; }
        }

        public override double ParagraphIndent
        {
            get { return 0.0; }
        }

        public override IList<TextTabProperties> Tabs 
        { 
            get { return null; } 
        }

        public override TextAlignment TextAlignment
        {
            get  { return TextAlignment.Left; }
        }

        public override TextDecorationCollection TextDecorations 
        { 
            get { return null; } 
        }

        public override TextMarkerProperties TextMarkerProperties
        {
            get { return null; }
        }

        public override TextWrapping TextWrapping
        {
            get { return TextWrapping.Wrap; }
        }
    }
}
