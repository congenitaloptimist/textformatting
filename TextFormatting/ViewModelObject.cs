﻿using System;
using System.Windows.Media;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextFormatting
{
    public class ViewModelObject : ViewModel
    {
        static protected Random rnd = new Random(Guid.NewGuid().GetHashCode());

        public ViewModelObject( )
        {
            
        }

        public double X
        {
            get { return m_X; }
            set { if (m_X != value) { m_X = value; OnPropertyChanged(); } }
        }

        public double Y
        {
            get { return m_Y; }
            set { if (m_Y != value) { m_Y = value; OnPropertyChanged(); } }
        }

        public double W
        {
            get { return m_W; }
            set { if (m_W != value) { m_W = value; OnPropertyChanged(); } }
        }

        public double H
        {
            get { return m_H; }
            set { if (m_H != value) { m_H = value; OnPropertyChanged(); } }
        }

        public Color Color
        {
            get { return m_color; }
            set { if (m_color != value) { m_color = value; FillBrush = new SolidColorBrush(value); OnPropertyChanged(); } }
        }

        public Brush FillBrush
        {
            get { return m_brush; }
            set { if (m_brush != value) { m_brush = value; OnPropertyChanged(); } }
        }

        public Color Stroke
        {
            get { return m_stroke; }
            set { if (m_stroke != value) { m_stroke = value; StrokePen = new Pen(new SolidColorBrush(value), 4); OnPropertyChanged(); } }
        }

        public Pen StrokePen
        {
            get { return m_pen; }
            set { if (m_pen != value) { m_pen = value; OnPropertyChanged(); } }
        }

        Color m_color;
        Brush m_brush;
        Color m_stroke;
        Pen m_pen;

        double m_X;
        double m_Y;
        double m_W;
        double m_H;
    }
}
