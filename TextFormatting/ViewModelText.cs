﻿using System;
using System.Windows.Media;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextFormatting
{
    public class TextState
    {
        ViewModelTextParagraph paragraph;

        public ViewModelTextParagraph Paragraph
        {
            get { return paragraph; }
            set { paragraph = value; }
        }

        String m_font = "Arial";

        public String FontFamily
        {
            set { m_font = value; }
            get { return m_font; }
        }

        double m_size = 12;

        public double FontSize
        {
            set { m_size = value; }
            get { return m_size; }
        }

        bool m_bold = false;

        public bool Bold
        {
            set { m_bold = value; }
            get { return m_bold; }
        }

        bool m_italic = false;

        public bool Italic
        {
            set { m_italic = value; }
            get { return m_italic; }
        }

        bool m_superscript = false;

        public bool SuperScript
        {
            set { m_superscript = value; }
            get { return m_superscript; }
        }

        double m_letterspace = 0;

        public double LetterSpacing
        {
            set { m_letterspace = value; }
            get { return m_letterspace; }
        }

        double m_xscale = 1.0;

        public double XScale
        {
            set { m_xscale = value; }
            get { return m_xscale; }
        }
    }

    public abstract class ViewModelText : ViewModel
    {
        public ViewModelText()
            : base()
        {

        }

        public abstract String Content { get; }
        public abstract int Index { get; set; }
        public abstract int Length { get; }

        TextState m_state;
        public TextState State
        {
            get
            {
                if (null == m_state)
                    m_state = new TextState();
                return m_state;
            }
        }
    }

    public class ViewModelTextRun : ViewModelText
    {
        public ViewModelTextRun( String text )
            : base()
        {
            m_content = text;
        }

        String m_content = "";
        String m_font = "Arial";
        double m_size = 12;
        bool m_bold = false;
        bool m_italic = false;
        double m_letterspace = 0;
        double m_xscale = 1.0;
        bool m_superscript = false;

        public override string Content
        {
            get { return m_content; }
        }

        public override int Index
        {
            get; set;
        }

        public override int Length
        {
            get { return m_content.Length; }
        }

        public String FontFamily
        {
            set { m_font = value; }
            get { return m_font; }
        }

        public double FontSize
        {
            set { m_size = value; }
            get { return m_size; }
        }

        public bool Bold
        {
            set { m_bold = value; }
            get { return m_bold; }
        }

        public bool Italic
        {
            set { m_italic = value; }
            get { return m_italic; }
        }

        public double LetterSpacing
        {
            set { m_letterspace = value; }
            get { return m_letterspace; }
        }

        public double XScale
        {
            set { m_xscale = value; }
            get { return m_xscale; }
        }

        public bool SuperScript
        {
            set { m_superscript = value; }
            get { return m_superscript; }
        }
    }

    public class ViewModelTextLineBreak : ViewModelText
    {
        public ViewModelTextLineBreak()
            : base()
        {

        }

        public override string Content
        {
            get { return "\n"; }
        }

        public override int Index
        {
            get; set;
        }

        public override int Length
        {
            get { return 1; }
        }
    }

    public class ViewModelTextParagraph : ViewModelText
    {
        public ViewModelTextParagraph()
            : base()
        {

        }

        public override string Content
        {
            get { return "\n"; }
        }

        public override int Index
        {
            get; set;
        }

        public override int Length
        {
            get { return 1; }
        }
    }
}