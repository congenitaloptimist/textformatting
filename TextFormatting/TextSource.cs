﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Media;
using System.Windows.Media.TextFormatting;
using System.Drawing;

/*
 * 
 */
namespace TextFormatting
{
    public class TextSourceAdapter : System.Windows.Media.TextFormatting.TextSource
    {
        public TextSourceAdapter(ViewModelObjectText model)
        {
            m_model = model;
        }

        public ViewModelObjectText State
        {
            get { return m_model; }
        }

        // Used by the TextFormatter object to retrieve a run of text from the text source.
        // @see https://docs.microsoft.com/en-us/dotnet/framework/wpf/advanced/advanced-text-formatting
        public override System.Windows.Media.TextFormatting.TextRun GetTextRun(int textSourceCharacterIndex)
        {
            // Make sure text source index is in bounds.
            if (textSourceCharacterIndex < 0)
                throw new ArgumentOutOfRangeException("TextSource", "Character index cannot be negative value");

            // Create and return either:
            // TextEndOfLine;
            // TextEndOfParagraph;
            // TextCharacters (derived)

            // FIXME: make sure the start paragraph is not found at index 0 (Count == 0)!!
            ViewModelText found = m_model.AtIndex(textSourceCharacterIndex);
            if (found == null)
                return new System.Windows.Media.TextFormatting.TextEndOfParagraph(1);

            if (found is ViewModelTextParagraph)
            {
                return new System.Windows.Media.TextFormatting.TextEndOfParagraph(1);
            }

            if (found is ViewModelTextLineBreak)
            {
                return new System.Windows.Media.TextFormatting.TextEndOfLine(1);
            }

            if (found is ViewModelTextRun)
            {
                    return new System.Windows.Media.TextFormatting.TextCharacters(
                        found.Content,
                        textSourceCharacterIndex - found.Index,
                        found.Content.Length - (textSourceCharacterIndex - found.Index),
                        new TextRunPropertiesAdapter(found as ViewModelTextRun));
            }

            // Return an end-of-paragraph if no more text source.
            return new System.Windows.Media.TextFormatting.TextEndOfParagraph(1);
        }

        public override TextSpan<CultureSpecificCharacterBufferRange> GetPrecedingText(int textSourceCharacterIndexLimit)
        {
            return null;
        }

        public override int GetTextEffectCharacterIndexFromTextSourceCharacterIndex(int textSourceCharacterIndex)
        {
            return textSourceCharacterIndex;
        }

        ViewModelObjectText m_model;
    }
}
