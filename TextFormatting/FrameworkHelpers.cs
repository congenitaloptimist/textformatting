﻿using System;
using System.Collections;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace TextFormatting
{
    static class FrameworkHelpers
    {
        public static T FindVisualChildByName<T>(DependencyObject parent, string childName)
            where T : DependencyObject
        {
            // Confirm parent and childName are valid. 
            if (parent == null)
                return null;

            T foundChild = null;

            int childrenCount = VisualTreeHelper.GetChildrenCount(parent);
            for (int i = 0; i < childrenCount; i++)
            {
                var child = VisualTreeHelper.GetChild(parent, i);
                
                // If the child is not of the requested type
                T childType = child as T;
                if (childType == null)
                {
                    // recursively drill down the tree
                    foundChild = FindVisualChildByName<T>(child, childName);

                    // If the child is found, break so we do not overwrite the found child. 
                    if (foundChild != null) break;
                }
                else if (!string.IsNullOrEmpty(childName))
                {
                    var frameworkElement = child as FrameworkElement;
                    // If the child's name is set for search
                    if (frameworkElement != null && frameworkElement.Name == childName)
                    {
                        // if the child's name is of the request name
                        foundChild = (T)child;
                        break;
                    }
                }
                else
                {
                    // child element found.
                    foundChild = (T)child;
                    break;
                }
            }

            return foundChild;
        }

        public static T FindVisualParent<T>(DependencyObject depObj) where T : DependencyObject
        {
            if (depObj != null)
            {
                DependencyObject parent = VisualTreeHelper.GetParent(depObj);
                while (parent != null)
                {
                    if (parent is T)
                    {
                        return parent as T;
                    }
                    parent = VisualTreeHelper.GetParent(parent);
                }
            }
            return null;
        }

        public static T FindVisualChild<T>(DependencyObject depObj) where T : DependencyObject
        {
            if (depObj != null)
            {
                for (int i = 0; i < VisualTreeHelper.GetChildrenCount(depObj); i++)
                {
                    DependencyObject child = VisualTreeHelper.GetChild(depObj, i);
                    if (child != null && child is T)
                    {
                        return (T)child;
                    }

                    T childItem = FindVisualChild<T>(child);
                    if (childItem != null) return childItem;
                }
            }
            return null;
        }

        public static T FindLogicalParent<T>(DependencyObject depObj) where T : DependencyObject
        {
            if (depObj != null)
            {
                DependencyObject parent = LogicalTreeHelper.GetParent(depObj);
                while (parent != null)
                {
                    if (parent is T)
                    {
                        return parent as T;
                    }
                    parent = LogicalTreeHelper.GetParent(parent);
                }
            }
            return null;
        }

        public static T FindLogicalChild<T>(DependencyObject depObj) where T : DependencyObject
        {
            if (depObj != null)
            {
                IEnumerable enumeration = LogicalTreeHelper.GetChildren(depObj);

                foreach (object child in enumeration)
                {
                    if (child != null && child is T)
                    {
                        return (T)child;
                    }

                    if (child is DependencyObject)
                    {
                        T childItem = FindLogicalChild<T>(child as DependencyObject);
                        if (childItem != null)
                            return childItem;
                    }
                }
            }
            return null;
        }

    }
}
